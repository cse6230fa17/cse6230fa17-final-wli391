# Final Projects

**Details of the checkpoints may be revised until they are assigned on T-Square.**

There are five problems to choose as project topics:

* Two problems with dense calculations:

    * [Discrete Fourier transform](Discrete-Fourier-Transform/)
    * [Matrix-matrix multiply-add](Matrix-Matrix-Multiply-Add/)

* Two problems with structured calculations:

    * [Lattice Boltzmann method](Lattice-Boltzmann-Method/)
    * [Gauss-Seidel iteration](Gauss-Seidel-Iteration/)

* One problem with unstructured calculations:

    * [Breadth-first search](Breadth-First-Search/)

## Proper Conduct 

* Teams may discuss their work with each other.
* Algorithms / results that you did not derive must be cited.
* All writing and code must be original:
    * External libraries like BLAS are prohibited without special permission.

## Checkpoint 0: Choose Your Project & Team

**Due: Friday, September 8**

* You may work in pairs if you'd like.

## Checkpoint 1: Serial Analysis & Implementation

**Due: Friday, September 29**

* Submission will be via repository transfer on bitbucket, submitted by 11:59:59 p.m.
* Teams name your repository with both gtusernames: `cse6230fa17-final-gtuser1-gtuser2`.

Report (5 points):

* PDF in the `serial` directory of your chosen topic.
* Include citations where relevant.
* Give some examples of scientific/enginering/real-world applications to which your kernel is relevant (1 points).
* Give asymptotic serial analysis (RAM model) (4 points):
    * What is the best asymptotic performance ("Big-O") of existing serial algorithms?
    * What is the theoretical best lower bound on asymptotic performance, if it is different?
    * What is the asymptotic performance of your code?
    * To the best of your ability, characterize the number of arithmetic and logical
      operations (additions/subtractions, multiplications, divisions, comparisons, etc.) of your code as a function of the size parameters of your problem.
        * i.e., if possible, give a non-asymptotic, concrete expression, like a polynomial.
    * To the best of your ability, characterize the space requirements in bytes of your code as a function of the size parameters of your problem (include temporary workspace)
        * i.e., if possible, give a non-asymptotic, concrete expression, like a polynomial.
   

Code (5 points):

* Write a valid serial code that performs your calculation.
* It must conform to an API that we will specify for each project choice in the `serial` directory.
    * Do not change the Makefile, the header files, or the test file
* We will test your code on deepthought much like project 1, but...
    * This time your code is structured as a *library*:
        * The test program know none of the details, only links against the library and calls the functions.
        * The test program depends on **PETSc**:
            * Modules available on deepthought: `module use /nethome/tisaac3/opt/deepthought/modulefiles` and `module load petsc/cse6230-single` or `module load petsc/cse6230-double`
            * On your own computer: follow instructions on [the PETSc website](http://www.mcs.anl.gov/petsc) (if you are doing the DFT project, configure with `--download-fftw` option)
    * Points deducted for code that is hard to follow (-2), does not run out of the box on deepthought (-1), does not pass (-3)
       

## Checkpoint 2: Parallel Analysis & Machine Choice

Report:

* Choose a computing platform for your parallel implementation:
    * It must be a platform that instructors/graders have access to.
    * If you're already thinking about something exotic, see us soon.
* Discuss the characteristics of the machine that will be most relevant to the
  performance of your code.
* Choose a parallel computing model that approximates your platform.
* Give pseudocode for your algorithm on your chosen model.
* Give asymptotic analysis including the relevant model parameters.

## Checkpoint 3: Parallel Implementation

Code:

* Write a valid parallel code that performs your calculation.
* It must conform to an API that we will specify for each project choice (modulo data layout).
* We will have a driver for running each code: your primary goal is correctness.
* Verify that instructors/graders can run the driver on your platform.

## Final: Parallel Optimization

Code:

* Submit your best performing code.

Report:

* Describe what optimizations you made and why.
* Justify your performance: report performance statistics that support that
  your *implementation* matches the performance predicted by your computing model.
