#if !defined(MMMA_SINGLE_H)
#define      MMMA_SINGLE_H

#include <stddef.h>

/** C := alpha * C + A * B.

  \param[in]     m      row size of C & A
  \param[in]     n      column size of C & B
  \param[in]     r      column size of A / row size of B
  \param[in]     alpha  scalar
  \param[in/out] C      m x n matrix, row major
  \param[in]     A      m x r matrix, row major
  \param[in]     B      r x n matrix, row major
*/
int matrix_matrix_multiply_add_single (size_t m, size_t n, size_t r, float alpha, float *C, const float *A, const float *B);

#endif
