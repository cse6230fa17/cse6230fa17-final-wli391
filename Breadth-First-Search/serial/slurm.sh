#!/bin/bash -l

#SBATCH -N 1
#SBATCH -t 0:20:00
#SBATCH -e build.log
#SBATCH -o build.log
#SBATCH --open-mode truncate
module use /nethome/tisaac3/opt/deepthought/modulefiles
module load petsc/cse6230-single

make clean
make

date

if [[ -x "test_bfs" ]]
then
  ./test_bfs
  date
fi

