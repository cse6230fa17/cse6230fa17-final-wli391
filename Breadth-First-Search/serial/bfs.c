#include <stdlib.h>
#include <stdio.h>
#include "bfs.h"
#include "bfs_impl.h"

struct _cse6230graph
{
    int64_t numberOfVertices;
    int** nodes;
    
};

struct queue {
    int *items;
    int front;
    int rear;
};

int graph_create (size_t num_edges, const int64_t (*edges)[2], cse6230graph *graph)
{
    cse6230graph g = NULL;
    g = (cse6230graph) malloc(sizeof(*g));
    if (!g) return 1;
    int64_t numberOfVertices = 0;
    int64_t i,j;
    
    // find the number of vertices
    for(i=0; i<num_edges; i++){
        if (numberOfVertices < edges[i][0])
            numberOfVertices = edges[i][0];
        if (numberOfVertices < edges[i][1])
            numberOfVertices = edges[i][1];
    }
    g->numberOfVertices = numberOfVertices;
    
    //create a numberOfVertices * numberOfVertices matrix
    g->nodes = (int**) malloc(sizeof(int*)*(numberOfVertices+1));
    for(i=0; i<numberOfVertices+1; i++){
        g->nodes[i] = (int*) malloc(sizeof(int64_t)*(numberOfVertices+1));
    }
    
    //initialize the graph
    for(i=0; i<numberOfVertices+1; i++){
        for(j=0; j<numberOfVertices+1; j++){
            g->nodes[i][j] = -1;
        }
    }
    
    //adding edges to the graph
    for(i=0; i< num_edges; i++){
        g->nodes[edges[i][0]][edges[i][1]] = 1;
        printf("i is %lld edge %lld %lld \n", i,edges[i][0] ,edges[i][1]);
        printf("i is %lld edge %lld %lld \n", i,edges[i][1] ,edges[i][0]);
        g->nodes[edges[i][1]][edges[i][0]] = 1;
        
    }
  *graph = g;
  return 0;
}

int graph_destroy (cse6230graph *graph)
{
  free(*graph);
  *graph = NULL;
  return 0;
}

struct Vertex {
    int label;
    int visited;
};

//queue variables


int64_t rear;
int64_t front;
int64_t queueItemCount;
int64_t tempParent;
//graph variables

//array of vertices
//struct Vertex* lstVertices[MAX];

//adjacency matrix
//int adjMatrix[MAX][MAX];

//vertex count
int vertexCount = 0;


//queue functions

void insert(int64_t data, int64_t* queue, int64_t* queueItemCount,int64_t* visitedQueue) {
    queue[++rear] = data;
    (*queueItemCount)++;
    visitedQueue[data]=1;
}

int64_t removeData(int64_t* front, int64_t* queue, int64_t queueItemCount) {
    queueItemCount--;
    return queue[*front++];
}



int breadth_first_search (cse6230graph graph, int num_keys, const int64_t *keys, int64_t **parents)
{
    int64_t i,j,k;
    //initialize the parents
    for(i=0; i<num_keys; i++){
        for(j=0; j<graph->numberOfVertices; j++){
            parents[i][j]=-1;
        }
    }
    
    for(i=0; i<num_keys; i++){
        int64_t key=keys[i];
        rear = -1;
        front = 0;
        queueItemCount = 0;
        tempParent = key;
        int64_t* queue =(int64_t*) malloc(sizeof(int64_t)*graph->numberOfVertices);
        int64_t* visitedQueue =(int64_t*) malloc(sizeof(int64_t)*graph->numberOfVertices);
        //initialize visited queue
        for(j=0; j<graph->numberOfVertices; j++){
            visitedQueue[j] =0;
        }
        
        //add start point into queue
        insert(key, queue, &queueItemCount, visitedQueue);
        //add neighbor of key to queue
        for(j=0; j<graph->numberOfVertices; j++){
            if(graph->nodes[key][j] == 1){
                insert(j, queue, &queueItemCount, visitedQueue);
            }
        }
        
        //get every parent of [i][j]
        for(j=0; j<graph->numberOfVertices; j++){
            //int* visited =(int*) malloc(sizeof(int)*graph->numberOfVertices);
            if(j==key){
                parents[i][j] = 1;
                continue;
                
            }
            
            while (queueItemCount!=0) {
                int64_t tempVertex = removeData(&front, queue, queueItemCount);
                if(tempVertex == j){
                    parents[i][j] = tempParent;
                }
                tempParent = tempVertex;
               
                //add temp vertex's neighbor into queue
                for(k=0; k<graph->numberOfVertices; k++){
                    if(visitedQueue[k] != 1){
                        insert(k, queue, &queueItemCount, visitedQueue);
                    }
                }
            }
            parents[i][j] = -1;

            
          
        }
        free(queue);
    }
  return 0;
}

